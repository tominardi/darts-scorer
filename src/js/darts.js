var dartScorerApp = angular.module('dartScorerApp', []);

dartScorerApp.controller('dartScorerController', ['$scope', function DartScorerController($scope) {

    $scope.players = [];
    $scope.currentPlayer = 0; //index of current player
    $scope.leftDart = 3;
    $scope.gameScore = 101;
    $scope.startingScoreForThisTurn = this.gameScore;
    $scope.gameState = "OVER";
    $scope.gameMode = "CLICKONTARGET"; // CLICKONTARGET, INBROWSER
    $scope.newPlayer = "";
    $scope.lastMessage = "";
    $scope.aimX = 0;
    $scope.aimY = 0;
    $scope.axisXmovement = null;
    $scope.axisYmovement = null;
    $scope.axisXdynamic = 1;
    $scope.axisYdynamic = 1;
    $scope.actionState = "STOPPED"; // WAITING | MOVINGX | MOVINGY | STOPPED

    $scope.isGameStarted = function () {
        return $scope.gameState === "STARTED";
    };

    $scope.removePlayer = function (index) {
        $scope.players.splice(index, 1);
    };
    $scope.addPlayer = function (newPlayer) {
        if (newPlayer !== "") {
            $scope.players.push({'name': newPlayer, 'score': $scope.gameScore});
            /**
             * The following line is temporary,
             * it cause bug because it doesn't really update the value
             * If you click two times on the button, it keep to add the same player again and again
             */
            document.getElementById('new-player').value = ""; // TODO use AngularJS instead of pure JS
        }
    };

    $scope.modifyMode = function(item) {
        $scope.gameMode = item.gameMode;
    };

    $scope.startGame = function () {
        if ($scope.players.length > 0) {
            $scope.players.forEach(player => {
                player.score = $scope.gameScore;
            });
            $scope.gameState = "STARTED";
            $scope.actionState = "WAITING";
            if ($scope.gameMode === "INBROWSER") {
                document.getElementById("launcher").classList.add("visible");
            }
        } else {
            alert("Merci d'inscrire au moins un joueur.");
        }
    };

    $scope.winGame = function () {
        $scope.gameState = "OVER";
        $scope.actionState = "STOPPED";
        document.getElementById("launcher").classList.remove("visible");
        $scope.lastMessage = "";
        alert($scope.players[$scope.currentPlayer].name + " a gagné la partie !");
    };

    $scope.nextPlayer = function () {
        if ($scope.isGameStarted()) {
            $scope.currentPlayer += 1;
            if ($scope.currentPlayer >= $scope.players.length) {
                $scope.currentPlayer = 0;
            }
            $scope.leftDart = 3;
            $scope.startingScoreForThisTurn = $scope.players[$scope.currentPlayer].score;
            $scope.sendMessage("Le tour est terminé. Joueur suivant s'il vous plait", true);
        }
    };
    $scope.clickHit = function (score) {
        if ($scope.gameMode === "CLICKONTARGET") {
            $scope.hit(score);
        }
    };
    $scope.hit = function (score) {
        if ($scope.isGameStarted()) {
            $scope.sendMessage("Vous avez marqué " + score + " points !");
            if ($scope.players[$scope.currentPlayer].score - score > 0) {
                $scope.players[$scope.currentPlayer].score = $scope.players[$scope.currentPlayer].score - score;
                $scope.leftDart -= 1;
                if ($scope.leftDart === 0) {
                    $scope.nextPlayer();
                }
            }
            else if ($scope.players[$scope.currentPlayer].score - score === 0) {
                $scope.winGame();
            }
            else {
                $scope.sendMessage('Trop élevé, le score reste à ' + $scope.startingScoreForThisTurn);
                $scope.players[$scope.currentPlayer].score = $scope.startingScoreForThisTurn;
                $scope.nextPlayer();
            }
        }
    };

    $scope.getNumber = function(num) {
        return new Array(num);
    };

    /**
     * TODO: archive messages
     * @param String message
     */
    $scope.sendMessage = function(message, append) {
        if (append) {
            $scope.lastMessage += ' ' + message;
        } else {
            $scope.lastMessage = message;
        }
    };

    $scope.updateGoal = function(score) {
        $scope.gameScore = score;
        $scope.sendMessage($scope.gameScore);
    };

    /**
     * Is this method really necessary ?
     * @param newScope
     */
    $scope.modifyScore = function(newScope) {
        $scope.gameScore = newScope.gameScore;
        $scope.sendMessage($scope.gameScore);
    };

    $scope.moveAxisX = function() {
        $scope.axisXmovement = setInterval(function() {
            if($scope.axisXdynamic > 0) {
                $scope.aimX += 2;
                if ($scope.aimX >= 100) {
                    $scope.axisXdynamic = -1;
                }
            } else {
                $scope.aimX -= 2;
                if ($scope.aimX <= 0) {
                    $scope.axisXdynamic = 1;
                }
            }
            document.getElementById('arrow-x').style.left = $scope.aimX+'%';
        }, 20);
        $scope.actionState = "MOVINGX";
    };

    $scope.stopMovementAxisX = function() {
        if ($scope.axisXmovement) {
            clearInterval($scope.axisXmovement);
        }
    };

    $scope.moveAxisY = function() {
        $scope.axisYmovement = setInterval(function() {
            if($scope.axisYdynamic > 0) {
                $scope.aimY += 1;
                if ($scope.aimY >= 100) {
                    $scope.axisYdynamic = -1;
                }
            } else {
                $scope.aimY -= 1;
                if ($scope.aimY <= 0) {
                    $scope.axisYdynamic = 1;
                }
            }
            document.getElementById('arrow-y').style.bottom = $scope.aimY+'%';
        }, 20);
        $scope.actionState = "MOVINGY";
    };

    $scope.stopMovementAxisY = function() {
        if ($scope.axisYmovement) {
            clearInterval($scope.axisYmovement);
        }
    };

    $scope.animateDart = function(X, Y) {
        return new Promise((resolve, reject) => {
            resolve({'X': X, 'Y': Y})
          });
    };

    $scope.animateScore = function() {
        setTimeout(function() {}, 2000);
    };

    $scope.launchDart = function() {
        console.log('je lance la flechette aux coordonnées ' + $scope.aimX + ' / ' + $scope.aimY);
        var wrapper = document.getElementById('dartboard-wrapper');
        var wrapperHeight = wrapper.offsetHeight;
        var wrapperWidth = wrapper.offsetWidth;
        var wrapperX = wrapper.offsetLeft;
        var wrapperY = wrapper.offsetTop;
        var clickX = (($scope.aimX * wrapperWidth) / 100) + wrapperX;
        var clickY = (($scope.aimY * wrapperHeight) / 100) + wrapperY;
        $scope.animateDart(clickX, clickY)
            .then(function(position) {
                var hited = document.elementFromPoint(position.X, position.Y);
                if(hited.getAttribute('points')) {
                    $scope.hit(hited.getAttribute('points'))
                } else {
                    // if miss target, then we just hit 0
                    $scope.hit(0);
                }
            })
            .then($scope.animateScore);
    };

    $scope.reinitializeLauncher = function() {
        $scope.actionState = "WAITING";
        $scope.aimX = 0;
        $scope.aimY = 0;
        document.getElementById('arrow-x').style.left = $scope.aimX+'%';
        document.getElementById('arrow-y').style.bottom = $scope.aimY+'%';
    };

    $scope.actionLauncher = function() {
        if ($scope.actionState == "WAITING") {
            $scope.moveAxisX();
        } else if ($scope.actionState == "MOVINGX") {
            $scope.stopMovementAxisX();
            $scope.moveAxisY();
        }
        else if ($scope.actionState == "MOVINGY") {
            $scope.stopMovementAxisY();
            $scope.launchDart();
            $scope.reinitializeLauncher();
        }
    };

}]);
