# Darts Scorer #

Darts scorer is an AngularJS application to count scores in Darts game

### Game ###

The goal of this game is to reach a score (101, 301, 501) the most quickly as possible.

### Goal ###

By default, three goals are available : 101, 301, 501. But it can be setted as wanted by the players.

### Players ###

We can register as many player as wanted. Each players plays alternately.

### Turns ###

Three darts at each turns.

The player write the score he does, and his total is subtract to his score.

If his score is bigger than his total left, he keeps his initial score. ("Player A has 14 points left, he score 18 points, his score is still 14).

When a player has a score exactly equals at 0, he win the game.

### How to play ###

Just open the *src/html/index.html* file in your browser.